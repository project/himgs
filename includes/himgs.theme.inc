<?php

/**
 * Implements hook_preprocess_himgs().
 */
function template_process_himgs_image_block(&$variables) {
  $variables['image'] = theme('image', array('path' => $variables['image_path']));
  $variables['himgs_html_id'] = 'himgs-image-block-' . $variables['himgs_entity']->id;
  $variables['classes'] = isset($variables['classes'])?$variables['classes']:'himgs-main-title';
  if($variables['title_classes']){
    $variables['classes'] .= $variables['title_classes'];
  }
}
