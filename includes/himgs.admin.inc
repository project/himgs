<?php

/**
 * Head images entity creation form
 */
function himgs_form($form, &$form_state, $himgs, $op = 'edit') {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Head image name'),
    '#required' => TRUE,
    '#default_value' => $himgs->name,
  );
  $form['pattern'] = array(
    '#type' => 'textarea',
    '#title' => t('Page paths pattern'),
    '#description' => t('Set pattern of file names wich want to process'),
    '#required' => TRUE,
    '#default_value' => $himgs->pattern,
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#delta' => 20,
    '#default_value' => $himgs->weight,
    '#title' => t('Wight for priority'),
  );
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  $form['settings']['use_page_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use page title'),
    '#description' => t('Use page title as image block title'),
    '#default_value' => $himgs->settings['use_page_title'],
  );
  $form['settings']['display_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for display'),
    '#default_value' => $himgs->settings['display_title'],
    '#states' => array(
      'invisible' => array(
        ':input[name="settings[use_page_title]"]' => array('checked' => TRUE),
      )
    ),
  );
  $form['settings']['hide_page_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide page title'),
    '#default_value' => $himgs->settings['hide_page_title'],
  );
  $form['settings']['title_tag'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('Title tag'),
    '#default_value' => $himgs->settings['title_tag'],
  );
  $form['settings']['title_classes'] = array(
    '#type' => 'textfield',
    '#title' => t('Title classes'),
    '#default_value' => $himgs->settings['title_classes'],
  );
  $form['settings']['title_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Text over image'),
    '#description' => t('Text over image after title, allowed html'),
    '#default_value' => $himgs->settings['title_text'],
  );

  $form['id'] = array(
    '#type' => 'value',
    '#value' => $himgs->id,
  );
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $himgs->type,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  field_attach_form('himgs', $himgs, $form, $form_state);
  return $form;
}

/**
 * Submit callback for himgs_form
 */
function himgs_form_submit(&$form, &$form_state) {
  $himgs = entity_ui_form_submit_build_entity($form, $form_state);
  $himgs->save();
  $form_state['redirect'] = 'admin/structure/himgs';
}
