<?php

/**
 * Implements hook_permission().
 */
function himgs_permission() {
  return array(
    'administer himgs' => array(
      'title' => t('Add and edit head images blocks'),
    )
  );
}

/**
 * Implements hook_entity_info().
 */
function himgs_entity_info() {
  $return = array(
    'himgs' => array(
      'label' => t('Head images'),
      'entity class' => 'Entity',
      'controller class' => 'himgsController',
      'base table' => 'himgs',
      'uri_callback' => 'himgs_uri',
      'fieldable' => TRUE,
      'entity keys' => array(// we are setting the entity keys and the bundle as the property named 'type' of this entity.
        'id' => 'id',
        'label' => 'name',
        'pattern' => 'pattern',
        'bundle' => 'type',
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Default'),
          'custom settings' => FALSE,
        ),
      ),
      'bundle keys' => array(
        'bundle' => 'type',
      ),
      'bundles' => array(// define the bundles in the entity
        'main' => array(
          'label' => t('main', array(), array('context' => 'Himgs main bundle'))
        ),
      ),
      'admin ui' => array(
        'path' => 'admin/structure/himgs',
        'file' => 'includes/himgs.admin.inc',
        'controller class' => 'himgsUiController',
      ),
      'access callback' => 'himgs_access',
      'module' => 'himgs',
    ),
  );

  return $return;
}

/**
 * Entity access check.
 */
function himgs_access($op, $entity, $account = NULL, $entity_type = 'himgs') {
  return user_access('administer himgs');
}

/**
 * Himgs entity load.
 */
function himgs_load($id) {
  $entities = entity_load('himgs', array($id));
  return $entities ? array_shift($entities) : FALSE;
}

/**
 * Helper function for sorting entities.
 */
function _himgs_sort_rows_by_weight($a, $b) {
  if ($a[1] == $b[1]) {
    return 0;
  }
  return ($a[1] > $b[1]) ? 1 : -1;
}

/**
 * Load himgs entity for current page.
 * @return type
 */
function himgs_get_entity_for_current_page() {
  $himgs_enitity = &drupal_static(__FUNCTION__);
  if (is_null($himgs_enitity)) {
    $himgs_enitity = FALSE;
    $items = db_select('himgs', 'h')
            ->fields('h', array('id', 'pattern'))
            ->orderBy('h.weight', 'ASC')
            ->execute()->fetchAll();
    foreach ($items as $item) {
      if (himgs_match_path($item->pattern)) {
        $himgs_enitity = himgs_load($item->id);
        break;
      }
    }
    drupal_alter('himgs_select_block', $himgs_enitity);
  }
  return $himgs_enitity;
}

/**
 * Chek pages matching current path
 * @param string $pages
 * @return bool matched
 */
function himgs_match_path($pages) {
  $pages = drupal_strtolower($pages);
  $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
  $page_match = drupal_match_path($path, $pages);
  if ($path != $_GET['q']) {
    $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
  }
  return $page_match;
}

/**
 * Implements hook_preprocess_page().
 */
function himgs_preprocess_page(&$vars) {
  $himgs_entity = himgs_get_entity_for_current_page();
  // Hide title
  if ($himgs_entity && $himgs_entity->settings['hide_page_title']) {
    $vars['title'] = FALSE;
  }
}

/**
 * Implements hook_block_info().
 */
function himgs_block_info() {
  $blocks = array();
  $blocks['himgs'] = array(
    'info' => t('Head image block'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function himgs_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'himgs':
      $block['subject'] = NULL;
      $block['content'] = himgs_view_head_image_block();
      break;
  }
  return $block;
}

function himgs_view_head_image_block() {
  $himgs_entity = himgs_get_entity_for_current_page();
  if (!$himgs_entity) {
    return array();
  }
  $settings = $himgs_entity->settings;
  $variables = array();
  $wrapper = entity_metadata_wrapper('himgs', $himgs_entity);
  $image = $wrapper->himgs_image->value();
  $variables['image_path'] = file_create_url($image['uri']);

  if ($settings['use_page_title']) {
    $variables['display_title'] = drupal_get_title();
  }
  $variables += $settings;
  unset($variables['use_page_title']);
  unset($variables['hide_page_title']);
  $output = array(
    '#theme' => 'himgs_image_block',
  );
  foreach ($variables as $key => $value) {
    $output['#' . $key] = $value;
  }
  $output['#himgs_entity'] = $himgs_entity;
  //Allow other modules to alter view data.
  drupal_alter('himgs_block_view', $output);
  return $output;
}

/**
 * Implements hook_theme().
 */
function himgs_theme() {
  return array(
    'himgs_image_block' => array(
      'variables' => array(
        'himgs_entity' => NULL,
        'title_tag' => 'div',
        'display_title' => NULL,
        'title_classes' => NULL,
        'title_text' => NULL,
        'image_path' => NULL,
      ),
      'file' => 'includes/himgs.theme.inc',
      'template' => 'himgs-image-block',
    ),
  );
}
