<div id="<?php print $himgs_html_id; ?>" class="himgs-image-block-wrapper">
  <div class="himgs-image"><?php print $image; ?></div>
  <<?php print $title_tag; ?> class="<?php print $classes; ?>"><?php print $display_title; ?></<?php print $title_tag; ?>>
  <div class="himgs-subtitle"><?php print $title_text; ?></div>
</div>