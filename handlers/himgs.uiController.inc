<?php

class himgsUiController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage head images items.';
    return $items;
  }

  protected function overviewTableHeaders($conditions, $rows, $additional_header = array()) {
    $additional_header = array(
      'image' => t('Image'),
      'weight' => t('Weight'),
      'pattern' => t('pattern'),
    );
    return parent::overviewTableHeaders($conditions, $rows, $additional_header);
  }

  protected function overviewTableRow($conditions, $id, $entity, $additional_cols = array()) {
    $image_display_options = array(
      'label' => 'hidden',
      'settings' => array(
        'image_style' => 'thumbnail'
      )
    );
    $image_field = field_view_field('himgs', $entity, 'himgs_image', $image_display_options);
    $additional_cols = array(
      'image' => render($image_field),
      'weight' => $entity->weight,
      'pattern' => nl2br($entity->pattern),
    );
    return parent::overviewTableRow($conditions, $id, $entity, $additional_cols);
  }

  public function overviewTable($conditions = array()) {
    $render = parent::overviewTable();
    usort($render['#rows'], '_himgs_sort_rows_by_weight');
    return $render;
  }

}
