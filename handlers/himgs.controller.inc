<?php

class himgsController extends EntityAPIController {

  /**
   * Implements EntityAPIControllerInterface::create().
   */
  public function create(array $values = array()) {
    global $user;
    $values += array(
      'id' => '',
      'weight' => 0,
      'name' => '',
      'pattern' => '',
      'type' => 'main',
      'settings' => array(
        'display_title' => '',
        'hide_page_title' => FALSE,
        'use_page_title' => FALSE,
        'title_text' => '',
        'title_tag' => 'h1',
        'title_classes' => '',
      )
    );

    return parent::create($values);
  }

}
